<aside class="sidebar">
	<section class="form">
		<h2><?php the_field('sidebar_head' , 'options'); ?></h2>
		<h4><?php the_field('sidebar_info' , 'options'); ?></h4>				
		<?php gravity_form(1, false, false, false, '', false); ?>
	</section>
	<article>
		<h2>In The News</h2>
		<ul>
			<?php
			 $postslist = get_posts('numberposts=3&order=DESC&orderby=date');
			 foreach ($postslist as $post) :
			    setup_postdata($post);
			 ?>
			 <li>
			 <h4><?php the_time(get_option('date_format')) ?></h4>
			 <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			 <?php the_excerpt(); ?>
			 <a href="<?php the_permalink(); ?>" class="read-more">read more</a>
			 </li>
			 <?php endforeach; 
			 	   wp_reset_query();
			 ?>
		</ul>
	</article>	
</aside>