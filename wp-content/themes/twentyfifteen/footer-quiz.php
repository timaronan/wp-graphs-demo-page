<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<footer></footer>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/quiz.js"></script>

</body>
</html>
