jQuery(document).ready(function($) {

    $('#menu-cntrl').on('click', function(){
        if( $('nav').hasClass('open') ){
            $('nav').addClass('closing');
            setTimeout(function(){
                $('nav').removeClass('closing');
            }, 250);
        }
        $('nav').toggleClass('open');
    });

});