<?php
/** Template Name: DIMPLE
 */
?>
<?php get_header( 'dimple' ); ?>
<script src="http://d3js.org/d3.v3.min.js"></script>
<script src="http://dimplejs.org/dist/dimple.v2.1.2.min.js"></script>
<style type="text/css">
	.chartWindow {
	  overflow-x: auto;
	  max-width: 100%;
	}
	<?php the_field('styles'); ?>
</style>
<section class="main">

	<h1 class="quiz-head">
		<?php if( get_field('header') ){ ?>
			<?php the_field('header'); ?>
		<?php }else{ ?>
			<?php the_title(); ?>
		<?php } ?>
	</h1>

	<article>
		
		<?php
			$cur = 0;
			if( have_rows('dimple_sections') ):
			while ( have_rows('dimple_sections') ) : the_row();

			        if( get_row_layout() == 'text_area' ){
						$cur += 1;
			        	?>
			        	<section class="text<?php echo $cur; ?>">
			        		<div class="text-area"> 
				        		<?php the_sub_field('wysiwyg'); ?> 
				        	</div>
			        	</section>
			        <?php
			        }elseif( get_row_layout() == 'graph' ){
						$cur += 1;
			        	$width = get_sub_field('width');
			        	$height = get_sub_field('height');
			        	$data_link = get_sub_field('data_link');
			        	$dlt = explode( '.', $data_link );
			        	$data_type = array_pop($dlt);

			        	$xaxis = get_sub_field('xaxis');
			        	$yaxis = get_sub_field('yaxis');
			        	$zaxis = get_sub_field('zaxis');
			        	if($xaxis){
			        		$lxa = end($xaxis);
			        	}
			        	if($yaxis){
			        		$lya = end($yaxis);
			        	}

			        	$xaxisval = get_sub_field('xaxisval');
			        	$yaxisval = get_sub_field('yaxisval');	
			        	$zaxisval = get_sub_field('zaxisval');

			        	$order_rule = get_sub_field('order_rule');
			        	$graph_type = get_sub_field('graph_type');
			        	$customs = get_sub_field('custom_scripts');

			        	$colors = get_sub_field('colors');
			        	if($colors){
			        		$lastColor = end($colors);
			        	}

			        	$series = get_sub_field('series');
			        	if($series){
			        		$lastSeries = end($series);
			        	}

			        	$vxa = get_sub_field('vxa');
			        	$vya = get_sub_field('vya');
			        	$vza = get_sub_field('vza');		        	
			        	?>
			        	<section class="graph<?php echo $cur; ?>">
			        		<div id="graph<?php echo $cur; ?>" class="chartWindow"></div>	        
			        	</section>
					<script type='text/javascript'>
						<?php
						echo "var svg". $cur ." = dimple.newSvg('#graph". $cur ."', ". $width .", ". $height .");";
					    echo "d3.". $data_type ."('".$data_link ."', function (data) {";
					    if( have_rows('add-ons') ):
						while ( have_rows('add-ons') ) : the_row();
					        if( get_row_layout() == 'filter' ){
					        	$ff = get_sub_field('ff');
					        	$fv = get_sub_field('fv');
					        	$lf = end($fv);
						    	echo "data = dimple.filterData(data, '". $ff ."', [";
								foreach($fv as $f){
									if($f == $lf) {
										echo "'". $f['value'] ."'";
								    }else{
										echo "'". $f['value']."',";
								    }
								}
						    	echo "]);";
					        }
						endwhile;
						endif;
					    echo "var myChart = new dimple.chart(svg". $cur .", data);";
						if($colors){
							echo "myChart.defaultColors = [";
							foreach($colors as $color){
								if($color == $lastColor) {
									echo "new dimple.color('". $color['color'] ."', '". $color['stroke'] ."', 1)";
							    }else{
									echo "new dimple.color('". $color['color'] ."', '". $color['stroke'] ."', 1),";
							    }
							}
							echo "];";
						}
					    echo "var x = myChart.add". $xaxisval ."Axis('x', [";
								foreach($xaxis as $xav){
									if($xav == $lxa) {
										echo "'". $xav['val'] ."'";
								    }else{
										echo "'". $xav['val'] ."',";
								    }
								}
						echo "]);";

					    echo "var y = myChart.add". $yaxisval ."Axis('y', [";
								foreach($yaxis as $yav){
									if($yav == $lya) {
										echo "'". $yav['val'] ."'";
								    }else{
										echo "'". $yav['val'] ."',";
								    }
								}
						echo "]);";

					    if($vza){
			        		$lza = end($zaxis);
						    echo "var z = myChart.add". $zaxisval ."Axis('z', [";
									foreach($zaxis as $zav){
										if($zav == $lza) {
											echo "'". $zav['val'] ."'";
									    }else{
											echo "'". $zav['val'] ."',";
									    }
									}
							echo "]);";
					    }
						if($graph_type != 'pie'){
							if($series){
								echo "var s = myChart.addSeries([";
								foreach($series as $ser){
									if($ser == $lastSeries) {
										echo "'". $ser['val'] ."'";
								    }else{
										echo "'". $ser['val'] ."',";
								    }
								}
								echo "], dimple.plot.". $graph_type .");";
							}else{
								echo "var s = myChart.addSeries(null, dimple.plot.". $graph_type .");";
							}	
						}	    
						if(!$vya){
							echo "y.hidden = true;";
						}
						if(!$vxa){
							echo "x.hidden = true;";
						}
						if( have_rows('add-ons') ):
						while ( have_rows('add-ons') ) : the_row();
					        if( get_row_layout() == 'inter' ){
					        	$cnt = get_sub_field('content');
					        	echo "s.interpolation = '". $cnt ."';";
					        }elseif( get_row_layout() == 'bounds' ){
					        	$left = get_sub_field('left');
					        	$right = get_sub_field('right');
					        	$top = get_sub_field('top');
					        	$bottom = get_sub_field('bottom');
					        	echo "myChart.setBounds(".$left.",".$top.",".$right.",".$bottom.");";
					        }elseif( get_row_layout() == 'legend' ){
					        	$left = get_sub_field('left');
					        	$width = get_sub_field('width');
					        	$top = get_sub_field('top');
					        	$height = get_sub_field('height');
					        	$placement = get_sub_field('placement');
					        	echo "myChart.addLegend(".$left.",".$top.",".$width.",".$height.", '".$placement."');";
					        }elseif( get_row_layout() == 'custom_scripts' ){
					        	$cnt = get_sub_field('content');
					        	echo $cnt;
					        }elseif( get_row_layout() == 'order_rule' ){
					        	$axis = get_sub_field('axis');
					        	$value = get_sub_field('value');
					        	$group = get_sub_field('group');
					        	if($group){
					        		echo $axis.".addGroupOrderRule('".$value."');";
					        	}else{
					        		echo $axis.".addOrderRule('".$value."');";
					        	}
					        }elseif( get_row_layout() == 'p-axis' ){
					        	$val = get_sub_field('val');
					        	echo "var p = myChart.addMeasureAxis('p', '". $val ."');";
					        	$sr = get_sub_field('series');
					        	if($sr){
					        		$vl = 0;
					        		echo "var pie". $vl ." = myChart.addSeries([";
									foreach($sr as $srs){
										$vl += 1;
										echo "'". $srs['value'] ."',";
									}
									echo "], dimple.plot.pie);";
					        	}
					        }elseif( get_row_layout() == 'ani' ){
								$cnt = get_sub_field('story');
								$ap = get_sub_field('autoplay');
								$ct = get_sub_field('controls');
								$sp = get_sub_field('speed');
								echo "var myStoryboard = myChart.setStoryboard('". $cnt ."');";
								if($ap){
									echo "myStoryboard.autoplay = true;";
								}else{
									echo "myStoryboard.autoplay = false;";
								}
								if($sp){
									echo "myStoryboard.frameDuration = ". $sp .";";
								}		
								if($ct){
									echo "var grp = d3.select('#graph". $cur ."');
									var play = grp.append('div').attr('class', 'play').text('play').on('click', function() {	
									    myStoryboard.startAnimation();
									});
									var pause = grp.append('div').attr('class', 'pause').text('pause').on('click', function() {
									    myStoryboard.pauseAnimation();
									});
									var stop = grp.append('div').attr('class', 'stop').text('stop').on('click', function() {
									    myStoryboard.stopAnimation();
									});
									";
								}													
					        }
						endwhile;
						endif;
      					echo "myChart.draw();
					    });"; ?>
					</script>

			        <?php
			        }
			    endwhile;

			else :

			    // no layouts found
			    echo "No content Found";

			endif;

		?>		

	</article>

</section>

<?php get_footer( 'dimple' ); ?>