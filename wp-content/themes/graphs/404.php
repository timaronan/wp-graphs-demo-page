<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php get_header(); ?>

<section class="the404">
	<h1>404</h1>	
	<h2>Page Not Found</h2>			
	<h3><?php bloginfo( 'name' ); ?><?php wp_title( '|' ); ?></h3>
</section>

<?php get_footer(); ?>