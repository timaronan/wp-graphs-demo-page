<?php
/** 
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */
?>
<?php get_header(); ?>

	<section id="primary" class="main">
		<div class="container">
			<h1>
				<?php if( get_field('header') ){ ?>
					<?php the_field('header'); ?>
				<?php }else{ ?>
					<?php the_title(); ?>
				<?php } ?>
			</h1>			
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; // end of the loop. ?>
		</div>
	</section><!-- #primary -->

<?php get_footer(); ?>