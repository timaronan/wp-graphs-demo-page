<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-graphs');

/** MySQL database username */
define('DB_USER', 'wp-graphs');

/** MySQL database password */
define('DB_PASSWORD', 'D!m913zzz');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}7Rx.5Cm0)vpoM?w!O!Q|D>BrOHMhjD@Br/k|bg}j7RE_I%cHi|ueen28xDT,2|l');
define('SECURE_AUTH_KEY',  'xqBTf&E:-(v;7;<BtnF.V&-++[g>f-Q:Mb$4MHiJI*mn|( O3WRVoiA0E&MBtg~w');
define('LOGGED_IN_KEY',    'V(3{v{ luG^/CVv2ro+K|yrXs+Kwk_<8%idKmkK|:8&?-YBP&4&N9$:1B%GI!Q?/');
define('NONCE_KEY',        'a96|1m#rT0WP:+0ct4?Rl-kc;gA-VBB+xz9v`tJ-{ya:?S)_c-&>RY--F6L7|![K');
define('AUTH_SALT',        '|g+IG,aT-3$Wp=gBVZE{HD-/(+4k3xn!lny+Nd9P-h$[!?^BLbP-~utZXc>T=[V|');
define('SECURE_AUTH_SALT', 'e^Ipe7mQinH&D{yoTr4fk%-1`!%~:&@sc7T)}j?--`Rm*2fr6[CkDcyy^I6Xza` ');
define('LOGGED_IN_SALT',   'S?hTG||tQ1cVEg|Be0p)P9@Rrz;`@xT_tHK<-lv$Ls%2IXc@1cC%>]ND>3beJWTr');
define('NONCE_SALT',       'Fi/jm G4~Bh+a~% Of1Pk(+/KHm]Ky(wy+hN0-:1?<w9c=i.t7}1H>JE:G1E4$M.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
