<aside class="sidebar news">
	<section class="form">
		<h2><?php the_field('sidebar_head' , 'options'); ?></h2>
		<h4><?php the_field('sidebar_info' , 'options'); ?></h4>				
		<?php gravity_form(1, false, false, false, '', false); ?>
	</section>
	<article>
		<?php 
		 // Custom widget Area Start
		 if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('News Sidebar') ) : ?>
		<?php endif;?>

		<section class="widget_search">
			<form role="search" method="get" id="searchform" class="searchform" action="http://172.28.1.141/ilawyer/chaffin-foundation/">
				<div>
				<input type="text" value  placeholder="Search" name="s" id="s" />
				<input type="submit" id="searchsubmit" class="submit" name="Submit" value="Go" />
				</div>
			</form>	
		</section>	
	</article>	
</aside>