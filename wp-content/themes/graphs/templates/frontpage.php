<?php
/** Template Name:Frontpage
 * The template for displaying the Home page.
 *
 * This is the template that displays the Home Page or Front Page of the site.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */
?>
<?php get_header(); ?>

<article>
	
	<h1>Simply Powerful</h1>
	<p>
		The aim of dimple is to open up the power and flexibility of d3 to analysts. 
		It aims to give a gentle learning curve and minimal code to achieve something productive. 
		It also exposes the d3 objects so you can pick them up and run to create some really cool stuff.
	</p>

</article>

<?php get_footer(); ?>