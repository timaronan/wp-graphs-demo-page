<?php
	/**
	 * Functions and definitions
	 */

	/* ========================================================================================================================
	
	Required external files
	
	======================================================================================================================== */
	
	/* ========================================================================================================================
	
	Theme specific settings
	
	======================================================================================================================== */

	add_theme_support('post-thumbnails');
	
	/* ========================================================================================================================
	
	Actions and Filters
	
	======================================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'starkers_script_enqueuer' );

	
	add_action('wp_footer','footer_scripts');

	/* ========================================================================================================================
	
	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
	
	======================================================================================================================== */


	/* ========================================================================================================================
	
	Scripts
	
	======================================================================================================================== */

	/**
	 * Add scripts via wp_head()
	 */

	function starkers_script_enqueuer() {
		
		wp_register_style( 'screen', get_stylesheet_directory_uri().'/stylesheets/screen.css', '', '', 'screen' );
        wp_enqueue_style( 'screen' );

	}

	// ADD SCRIPTS TO FOOTER HOOK
	function footer_scripts() {
		
		wp_deregister_script('jquery');
		wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
		wp_enqueue_script('jquery');
		
		wp_register_script( 'maps', 'https://maps.googleapis.com/maps/api/js?sensor=false', array( 'jquery' ) );
		wp_enqueue_script('maps');

		wp_register_script( 'site', get_template_directory_uri().'/js/site.js', array( 'jquery' ) );
		wp_enqueue_script('site');

		wp_register_script( 'wistia', '//fast.wistia.com/assets/external/popover-v1.js', array( 'jquery' ) );
		wp_enqueue_script('wistia');
	}
	
	/* ========================================================================================================================
	
	Comments
	
	======================================================================================================================== */

	
/* ========================================================================================================================
 Register Menus
======================================================================================================================== */
function register_my_menus() {
register_nav_menus( array(
		'primary_nav'  	=> 'Main Menu',
		'sidebar'  	=> 'Sidebar Menu',
		'practices'  	=> 'Practice Areas Menu'
	) );
}
add_action('init','register_my_menus');

/* ========================================================================================================================
 Widgets
======================================================================================================================== */
// register general widget
if(function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' 			=> 'General Widget',
		'id'     		=> 'general',
		'description'   => 'Widget for general use.'
	) );
}
// register new sidebar widget
if(function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' 			=> 'News Sidebar',
		'id'     		=> 'news-side',
		'description'   => 'Widget for the sidebar of the In the News Template.'
	) );
}

// Handle Gravity Forms inline scripts. This will allow you to AJAX forms
// While still enqueueing jQuery
add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
	$content = 'document.addEventListener( "DOMContentLoaded", function() { ';
	return $content;
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
	$content = ' }, false );';
	return $content;
}
// Add advanced custom fields options page
// This will be used for global Advanced Custom Fields
if( function_exists('acf_add_options_page') ) {
		acf_add_options_page();
}

add_filter('excerpt_length', 'my_excerpt_length');
function my_excerpt_length($length) {
return 20; // Or whatever you want the length to be.
}

?>