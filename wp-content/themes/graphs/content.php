<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>

		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
		<div class="featured-post">
			<?php _e( 'Featured post', 'duquetheme' ); ?>
		</div>
		<?php endif; ?>


		<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<article class="<?php //gets template name, removes .php, adds it as the class for the main tag
							$tempphp = get_post_meta( $id, '_wp_page_template', true );
							$tempname = substr($tempphp, 0, -4); 
							echo $tempname; ?> content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'warncke' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'warncke' ), 'after' => '</div>' ) ); ?>
		</article><!-- .entry-content -->
		<?php endif; ?>

