<?php
/** Template Name: QUIZ
 * This template runs a quiz. Requires quiz.js & custom fields dependancies.
 */
?>
<?php get_header( 'quiz' ); ?>

<style type="text/css">

	<?php 
		while( have_rows('colors') ): the_row(); 		
		$abg = get_sub_field('answer_bg');
		$ac = get_sub_field('answer_color');
		$ahbg = get_sub_field('answer_hover_bg');
		$acbg = get_sub_field('answer_correct_bg');
		$awbg = get_sub_field('answer_wrong_bg');
		$asbg = get_sub_field('answer_selected_bg');

		$sbg = get_sub_field('submit_bg');
		$sc = get_sub_field('submit_color');
		$shbg = get_sub_field('submit_hover_bg');
		$sc = get_sub_field('separator_color'); ?>

	.quiz-head{
		color: <?php echo $ac; ?>;
	}
	.quiz .options li, .quiz-menu li{
		background-color: <?php echo $abg; ?>;
		color: <?php echo $ac; ?>;
	}
	#quiz-correct, #quiz-percent{
		color: <?php echo $acbg; ?>;
	}
	#quiz-answered{
		color: <?php echo $asbg; ?>;
	}
	.quiz-menu li.current {
		background-color: <?php echo $asbg; ?>;
	}	
	.quiz .options li:hover, .quiz-menu li:hover{
		background-color: <?php echo $ahbg; ?>;
	}	
	.quiz .options li.checked, .quiz-menu li.checked{
		background-color: <?php echo $asbg; ?>;
	}
	.quiz .options li.incorrect.checked, .quiz-menu li.incorrect{
		background-color: <?php echo $awbg; ?>;
	}
	.quiz .options li.correct, .quiz .options li.correct.checked,
	.quiz-menu li.correct{
		background-color: <?php echo $acbg; ?>;
	}


	.quiz .question .submit{
		background-color: <?php echo $sbg; ?>;
		color: <?php echo $sc; ?>;
	}
	.quiz .question .submit:hover{
		background-color: <?php echo $shbg; ?>;
	}
	.quiz .question:not(:last-child){
		border-bottom: 1px solid <?php echo $sc; ?>;
	}
	#complete .social-media ul li path, #complete .social-media ul li polygon{
		fill: <?php echo $sbg; ?>;
	}


<?php endwhile; ?>	
</style>

<section class="main">
	
	<section class="quiz-container">
		<h1 class="quiz-head">
			<?php if( get_field('header') ){ ?>
				<?php the_field('header'); ?>
			<?php }else{ ?>
				<?php the_title(); ?>
			<?php } ?>
		</h1>

		<nav class="quiz-nav">
			
			<div class="container">
				
				<ul class="quiz-menu">
				<?php 
					$ques = 0;
					$answers = array();
					while( have_rows('question') ): the_row(); 		 
					// vars
					$ques = $ques + 1;
					?>	

					<li>
						
						<a href="#question<?php echo $ques; ?>"><?php echo $ques; ?></a>

					</li>
				 
				<?php endwhile; ?>	
				</ul>

				<h2 class="quiz-results">
					<span class="top">0 </span>
					/
					<span class="bottom"> 0</span>
				</h2>

			</div>

		</nav>

		<article id="quiz1" class="quiz">

			<?php 
				$ques = 0;
				$answers = array();
				while( have_rows('question') ): the_row(); 		 
				// vars
				$info = get_sub_field('info');
				$quest = get_sub_field('quest');
				$ques = $ques + 1;
				?>	

				<section id="question<?php echo $ques; ?>" class="question">
					
					<article>

						<p class="quest"><?php echo $ques; ?>. ) <?php echo $quest; ?></p>
						
						<ul class="options">

							<?php 
								$answ = 0;
								while( have_rows('answers') ): the_row();
								$answer = get_sub_field('answer');
								$correct = get_sub_field('correct');
								$answ = $answ + 1;
								if($correct){
									array_push($answers, $answ);
								}
								?>
								<li>
									<label>
										<input data-key="<?php echo $answ; ?>" type="radio" name="question<?php echo $ques; ?>">
										<?php echo $answer; ?>
									</label>
								</li>
							<?php endwhile; ?>

						</ul>

						<div class="submit">Submit</div>

					</article>

					<aside>
						<!-- <div class="close">&#8855;</div> -->
						<?php

							// check if the flexible content field has rows of data
							if( have_rows('info') ):

							     // loop through the rows of data
							    while ( have_rows('info') ) : the_row();

							        if( get_row_layout() == 'content' ){
							        	?>
							        	<div class="about"> 
							        		<?php the_sub_field('wysi');1 ?> 
							        	</div>
							        <?php
							        }elseif( get_row_layout() == 'image_upload' ){
							        	$img = get_sub_field('image');
							        	?>
							        	<div class="about"><img src="<?php echo $img; ?>"></iframe></div>
							        <?php
							        }elseif( get_row_layout() == 'image_link' ){
							        	$url = get_sub_field('url');
							        	?>
							        	<div class="about"><img src="<?php echo $url; ?>"></img></div>
							        <?php
							        }elseif( get_row_layout() == 'iframe' ){
							        	$url = get_sub_field('url');
							        	?>
							        	<div class="about"><iframe src="<?php echo $url; ?>"></iframe></div>
							        <?php
							        }


							    endwhile;

							else :

							    // no layouts found

							endif;

						?>
					<!-- <div class="reopen">
							<svg version="1.1" id="down-arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 1000 300" enable-background="new 0 0 1000 300" xml:space="preserve" preserveAspectRatio="xMinYMin meet" height="100%"
								 width="100%" class="down-arrow">
								<g>
										<path d="M982.7,10.3c0,16.1,0,31.3,0,46.5c0,20,0.2,20.2-42.2,25.8C797.8,101.5,655,120.2,512,138.5
									c-9.7,1.3-21.9,0.2-32.1-1.2c-149.6-19.3-299-38.8-448.4-58.5c-5.5-0.7-13.2-4.1-13.3-6.3c-1-19.5-0.5-39-0.4-58.6c0-1,1-2,2.2-4.2
									c20.9,2.6,41.4,5,61.7,7.7C219,35.3,356.2,53.4,493.8,70.6c11.8,1.5,26.9-1.2,39.8-2.8C679.9,48.9,826,29.9,972.1,10.9
									C974.3,10.7,976.6,10.7,982.7,10.3z"/>
								</g>
							</svg>
						</div> -->
					</aside>

				</section>
			 
			<?php endwhile; ?>	

			<script type='text/javascript'>
				<?php
				$js_array = json_encode($answers);
				$poop_array = array();
				$peep_array = array();
				foreach ($answers as &$value) {
				    array_push($poop_array, rand(1,4));
				    array_push($peep_array, rand(1,4));
				}
				$js_poop = json_encode($poop_array);
				$js_peep = json_encode($peep_array);

				echo "X = ". $js_array . ";\n";
				echo "Y = ". $js_poop . ";\n";
				echo "W = ". $js_peep . ";\n";
				?>
			</script>

		</article>

		<section id="complete">
			<div class="message">
				<p>You got <span id="quiz-correct">0</span> out of <span id="quiz-answered">0</span> right</p>
				<p>Your Score</p>
				<p class="quiz-perc"><span id="quiz-percent">0</span>%</p>
				<p>Share your score!</p>
			</div>
			<div class="social-media">
				
				<ul>
					<li class="facebook">
				        <a href="#" id="share-facebook" class="share-btn">
				            <span class="icon">
				                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="28px" height="28px" viewBox="0 0 28 28" enable-background="new 0 0 28 28" xml:space="preserve">
				                    <path d="M27.825,4.783c0-2.427-2.182-4.608-4.608-4.608H4.783c-2.422,0-4.608,2.182-4.608,4.608v18.434
				                        c0,2.427,2.181,4.608,4.608,4.608H14V17.379h-3.379v-4.608H14v-1.795c0-3.089,2.335-5.885,5.192-5.885h3.718v4.608h-3.726
				                        c-0.408,0-0.884,0.492-0.884,1.236v1.836h4.609v4.608h-4.609v10.446h4.916c2.422,0,4.608-2.188,4.608-4.608V4.783z"/>
				                </svg>
				            </span>
				            <!-- <span class="text">facebook</span> -->
				        </a>
				    </li>
				    <li class="twitter">
				        <a href="#" id="share-twitter" class="share-btn">
				            <span class="icon">
				                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				                     width="28px" height="28px" viewBox="0 0 28 28" enable-background="new 0 0 28 28" xml:space="preserve">
				                <path d="M24.253,8.756C24.689,17.08,18.297,24.182,9.97,24.62c-3.122,0.162-6.219-0.646-8.861-2.32
				                    c2.703,0.179,5.376-0.648,7.508-2.321c-2.072-0.247-3.818-1.661-4.489-3.638c0.801,0.128,1.62,0.076,2.399-0.155
				                    C4.045,15.72,2.215,13.6,2.115,11.077c0.688,0.275,1.426,0.407,2.168,0.386c-2.135-1.65-2.729-4.621-1.394-6.965
				                    C5.575,7.816,9.54,9.84,13.803,10.071c-0.842-2.739,0.694-5.64,3.434-6.482c2.018-0.623,4.212,0.044,5.546,1.683
				                    c1.186-0.213,2.318-0.662,3.329-1.317c-0.385,1.256-1.247,2.312-2.399,2.942c1.048-0.106,2.069-0.394,3.019-0.851
				                    C26.275,7.229,25.39,8.196,24.253,8.756z"/>
				                </svg>
				            </span>
				            <!-- <span class="text">twitter</span> -->
				        </a>
				    </li>
				    <li class="googleplus">
				        <!-- Replace href with your meta and URL information.  -->
				        <a href="#" id="share-google" class="share-btn">
				            <span class="icon">
				                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="28px" height="28px" viewBox="0 0 28 28" enable-background="new 0 0 28 28" xml:space="preserve">
				                    <g>
				                        <g>
				                            <path d="M14.703,15.854l-1.219-0.948c-0.372-0.308-0.88-0.715-0.88-1.459c0-0.748,0.508-1.223,0.95-1.663
				                                c1.42-1.119,2.839-2.309,2.839-4.817c0-2.58-1.621-3.937-2.399-4.581h2.097l2.202-1.383h-6.67c-1.83,0-4.467,0.433-6.398,2.027
				                                C3.768,4.287,3.059,6.018,3.059,7.576c0,2.634,2.022,5.328,5.604,5.328c0.339,0,0.71-0.033,1.083-0.068
				                                c-0.167,0.408-0.336,0.748-0.336,1.324c0,1.04,0.551,1.685,1.011,2.297c-1.524,0.104-4.37,0.273-6.467,1.562
				                                c-1.998,1.188-2.605,2.916-2.605,4.137c0,2.512,2.358,4.84,7.289,4.84c5.822,0,8.904-3.223,8.904-6.41
				                                c0.008-2.327-1.359-3.489-2.829-4.731H14.703z M10.269,11.951c-2.912,0-4.231-3.765-4.231-6.037c0-0.884,0.168-1.797,0.744-2.511
				                                c0.543-0.679,1.489-1.12,2.372-1.12c2.807,0,4.256,3.798,4.256,6.242c0,0.612-0.067,1.694-0.845,2.478
				                                c-0.537,0.55-1.438,0.948-2.295,0.951V11.951z M10.302,25.609c-3.621,0-5.957-1.732-5.957-4.142c0-2.408,2.165-3.223,2.911-3.492
				                                c1.421-0.479,3.25-0.545,3.555-0.545c0.338,0,0.52,0,0.766,0.034c2.574,1.838,3.706,2.757,3.706,4.479
				                                c-0.002,2.073-1.736,3.665-4.982,3.649L10.302,25.609z"/>
				                            <polygon points="23.254,11.89 23.254,8.521 21.569,8.521 21.569,11.89 18.202,11.89 18.202,13.604 21.569,13.604 21.569,17.004
				                                23.254,17.004 23.254,13.604 26.653,13.604 26.653,11.89      "/>
				                        </g>
				                    </g>
				                </svg>
				            </span>
				            <!-- <span class="text">google+</span> -->
				        </a>
				    </li>
				</ul>

			</div>
		</section>

	</section>

</section>

<?php get_footer( 'quiz' ); ?>